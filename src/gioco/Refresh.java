package gioco;

public class Refresh implements Runnable {

	private final int PAUSE = 3 ; // tempo di attesa per aggiornare lo schermo

	public void run() {
		// si ridesegna lo schermo ogni pausa millisecondi
		while (Piattaforma.mario.isVivo()){
			Main.scene.repaint();
			//Main.test.paintAll(Main.test.getGraphics());
			try {
				Thread.sleep(PAUSE);
			} catch (InterruptedException e) {
				//e.printStackTrace();
			}
		}
		System.exit(0);
	}
	
} 
