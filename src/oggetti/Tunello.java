 package oggetti;

import javax.swing.ImageIcon;

public class Tunello extends Ogetti {

	public Tunello(int X, int Y) {
		
		super(X, Y, 43 ,65);
		super.icoObj = new ImageIcon(getClass().getResource("/immagini/tunello.png"));
		super.imgObj = super.icoObj.getImage();
	}

}
